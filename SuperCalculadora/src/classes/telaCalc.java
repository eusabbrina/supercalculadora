package classes;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

public class telaCalc extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
//	protected static final String panCalc = null;
	private JPanel contentPane;
	private Panel panCalc = null;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					telaCalc frame = new telaCalc();
					frame.setVisible(true);
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public telaCalc() {
		setResizable(false);
		
		inicializarComponentes();
		
		panCalc.setVisible(false);
		
	}
	
	private void inicializarComponentes() {
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 534, 371);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Informe um valor:");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblNewLabel.setBounds(10, 39, 148, 17);
		contentPane.add(lblNewLabel);
		
		JSpinner txtNum = new JSpinner();
		txtNum.setModel(new SpinnerNumberModel(1, -50, 50, 1));
		txtNum.setBounds(185, 38, 61, 20);
		contentPane.add(txtNum);
		
		JLabel lblNewLabel_1 = new JLabel("");
		lblNewLabel_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_1.setIcon(new ImageIcon(telaCalc.class.getResource("/imagens/calculator-icon.png")));
		lblNewLabel_1.setBounds(318, 0, 208, 279);
		contentPane.add(lblNewLabel_1);
		
		
		panCalc = new Panel();
		panCalc.setBounds(30, 169, 248, 160);
		contentPane.add(panCalc);
		panCalc.setLayout(null);
		
		JLabel lblNewLabel_2 = new JLabel("Resto da Divis\u00E3o");
		lblNewLabel_2.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblNewLabel_2.setBounds(10, 25, 119, 14);
		panCalc.add(lblNewLabel_2);
		
		JLabel lblNewLabel_3 = new JLabel("Elevado ao Cubo");
		lblNewLabel_3.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblNewLabel_3.setBounds(10, 51, 119, 14);
		panCalc.add(lblNewLabel_3);
		
		JLabel lblNewLabel_4 = new JLabel("Raiz Quadrada");
		lblNewLabel_4.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblNewLabel_4.setBounds(10, 76, 119, 14);
		panCalc.add(lblNewLabel_4);
		
		JLabel lblNewLabel_5 = new JLabel("Raiz C\u00FAbica");
		lblNewLabel_5.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblNewLabel_5.setBounds(10, 101, 119, 14);
		panCalc.add(lblNewLabel_5);
		
		JLabel lblNewLabel_6 = new JLabel("Valor absoluto");
		lblNewLabel_6.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblNewLabel_6.setBounds(10, 122, 119, 14);
		panCalc.add(lblNewLabel_6);
		
		JLabel lblResto = new JLabel("0");
		lblResto.setHorizontalAlignment(SwingConstants.CENTER);
		lblResto.setForeground(new Color(0, 191, 255));
		lblResto.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblResto.setBounds(192, 24, 46, 14);
		panCalc.add(lblResto);
		
		JLabel lblCubo = new JLabel("0");
		lblCubo.setHorizontalAlignment(SwingConstants.CENTER);
		lblCubo.setForeground(new Color(0, 191, 255));
		lblCubo.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblCubo.setBounds(192, 51, 46, 14);
		panCalc.add(lblCubo);
		
		JLabel lblRaizq = new JLabel("0");
		lblRaizq.setHorizontalAlignment(SwingConstants.CENTER);
		lblRaizq.setForeground(new Color(0, 191, 255));
		lblRaizq.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblRaizq.setBounds(192, 76, 46, 14);
		panCalc.add(lblRaizq);
		
		JLabel lblRaizc = new JLabel("0");
		lblRaizc.setHorizontalAlignment(SwingConstants.CENTER);
		lblRaizc.setForeground(new Color(0, 191, 255));
		lblRaizc.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblRaizc.setBounds(192, 101, 46, 14);
		panCalc.add(lblRaizc);
		
		JLabel lblAbs = new JLabel("0");
		lblAbs.setHorizontalAlignment(SwingConstants.CENTER);
		lblAbs.setForeground(new Color(0, 191, 255));
		lblAbs.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblAbs.setBounds(192, 126, 46, 14);
		panCalc.add(lblAbs);
		
		JButton btnCalc = new JButton("Calcular");
		btnCalc.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			 
			int num = Integer.parseInt(txtNum.getValue().toString());
		    int r = num % 2;
		    lblResto.setText(Integer.toString(r));
			
		    double c = Math.pow(num, 3);
		    lblCubo.setText(Double.toString(c));
		    
		    double rq = Math.sqrt(num);
		    lblRaizq.setText(Double.toString(c));
		    
		    double rc = Math.cbrt(num);
		    lblRaizc.setText(String.format("%.2f", rc)); 
		    
		    int abs = Math.abs(num);
		    lblAbs.setText(Integer.toString(abs));
		    
		    panCalc.setVisible(true);
				
			}
		});
		btnCalc.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnCalc.setIcon(new ImageIcon(telaCalc.class.getResource("/imagens/ok.png")));
		btnCalc.setBounds(82, 70, 148, 79);
		contentPane.add(btnCalc);
		
	}

	
}

